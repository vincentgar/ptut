## PTUT

DEMARCHE A SUIVRE POUR PROJET TUTEURE:

- Lance braker avec nos genome téléchargé sur  NCBI (modifier le fichier braker3.sh avec les bons chemins SPECIES-GENOME-WKDIR)

- Extraire seulement les introns avec le Extract_intron.py à partir du fichier.gff3 --> creer fichier.txt

- Faire un ORF finder de STOP à STOP avec getorf.sh à partir du fichier.txt --> fichierORF.txt

- Faire un blastp avec le fichier blast.sh à partir du fichierORF.txt --> fichierORF_blastp.txt

- Faire un awk pour filtré le fichierORF_blastp.txt --> result.txt

- Utilise le script, Extract_intron2.2 avec en entrée le fichier result.txt et le fichier de l'ORF correspondant a la bonne espèce --> result2.txt

- Utilise InterpProScan, avec result2.txt dans le fichier test_interproscan-5.51-85.0.sh (interpro pour moi)

- Analyse les resultats en regardant les familles proteiques, et les goterms

- Analyse statistique 

- Corriger le fichier braker.gff3 avec correction_gff.py 


