#!/usr/bin/env python
import sys
def extract_pos_intron_gff(file_name):
    dict_intron = {}
    EDGES = 15
    with open(file_name, 'r') as file:
        for line in file:
            splited_line = line.split('\t')
            if splited_line[2] == "intron":
                sequence_name = splited_line[0]
                ""
                sequence_name = sequence_name[:3] + sequence_name[3:].replace('_', ' ')
                ""
                start = int(splited_line[3])
                if start > EDGES:
                    start -= EDGES
                end = int(splited_line[4])+EDGES
                if sequence_name in dict_intron:
                    dict_intron[sequence_name].append([start, end])
                else:
                    dict_intron[sequence_name] = [[start, end]]
    return dict_intron
# Renvoie un dictionnaire avec pour clé : nom de la séquences et pour valeur : liste de sous-listes (début,fin)
# Posibilité de modifier le nombre de bases à prendre en plus en début et fin de séquence avec EDGES


def extract_intron_sequences_from_fasta(fasta_file, dict_intron):
    intron_sequences = {}
    current_sequence_name = None
    current_sequence = ''

    with open(fasta_file, 'r') as file:
        for line in file:
            if line.startswith('>'):
                if current_sequence_name is not None:
                    if current_sequence_name in dict_intron:
                        introns_pos = dict_intron[current_sequence_name]
                        for intron in introns_pos:
                            start, end = intron
                            intron_sequence = current_sequence[start:end]
                            if current_sequence_name in intron_sequences:
                                intron_sequences[current_sequence_name].append(intron_sequence)
                            else:
                                intron_sequences[current_sequence_name] = [intron_sequence]
                current_sequence_name = line.strip()[1:]
                current_sequence = ''
            else:
                current_sequence += line.strip()

        if current_sequence_name is not None and current_sequence_name in dict_intron:
            introns_pos = dict_intron[current_sequence_name]
            for intron in introns_pos:
                start, end = intron
                intron_sequence = current_sequence[start:end]
                if current_sequence_name in intron_sequences:
                    intron_sequences[current_sequence_name].append(intron_sequence)
                else:
                    intron_sequences[current_sequence_name] = [intron_sequence]

    return intron_sequences


# Renvoie un dictionnaire avec pour clé : nom de la séquence et pour valeur : une liste contenant les séquences des introns


def write_introns(intron_sequences, output_file):
    with open(output_file, 'w') as f:
        for sequence_name, sequences in intron_sequences.items():
            for i in range(len(sequences)):
                f.write('>' + sequence_name + '_intron_' + str(i+1) + '\n')
                f.write(sequences[i] + '\n')


# Extraction des positions des introns de chaque séquences à partir d'un fichier gff
# Mettre en paramètre le chemin d'accès du ficher gff
braker_file = sys.argv[1]
dict_intron = extract_pos_intron_gff(braker_file)
# Extraction des séquences nucléiques des introns à partir d'un fichier FASTA et des positions obtenus précédemment 
# Mettre en paramètre le chemin d'accès du fichier fasta et les postitions des introns
fasta_file = sys.argv[2]
intron_sequences = extract_intron_sequences_from_fasta(fasta_file, dict_intron)
# Ecriture des séquences dans un fichier au format FASTA
# Mettre en paramètre les séquences et le chemin vers le fichier de sortie
out_file =sys.argv[3]
write_introns(intron_sequences,out_file)
